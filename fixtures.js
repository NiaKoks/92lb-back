const nanoid = require ("nanoid");

const mongoose = require('mongoose');
const Message = require('./models/Message');
const User =require('./models/User');
const config = require('./config');

const run =async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for(let collection of collections){
        await collection.drop();
    }

    const [message] = await Message.create(
        { user: user1._id,
          datetime: Date(),
          text: "Some text for test message"
        },
        { user: user2._id,
            datetime: Date(),
            text: "Some text for test message"
        },
    );

    const [user1,user2] = await User.create(
        { username: 'TestUser',
          password: '1234',
          role: 'user',
          token: nanoid(),
          status:'online'
        },
        { username: 'Admin',
          password: '2345',
          role: 'admin',
          token: nanoid(),
          status:'online'
        },
    );

    return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});