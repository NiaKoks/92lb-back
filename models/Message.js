const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MessageSchema= new Schema({
    user: {
        type: Schema.ObjectId,
        ref: "User",
        required: true
    },
    datetime: {
        type:Date,
        required:true
    },
    text: {
        type:String,
        required: true
    }
});


const Message = mongoose.model('Message', MessageSchema);

module.exports = Message;