const express= require('express');
const User = require('../models/User');


    const router = express.Router();
    router.post('/',async (req,res)=>{
        const user = new User (req.body);

        user.generateToken();

        try {
          await user.save();
          return res.send({message: 'Register successful', user});
        } catch(error){res.status(400).send(error)
        }
    });

    router.post('/sessions', async (req, res)=>{
        console.log(req.body.user);
        const user = await User.findOne({username:req.body.username});

        if(!user){
            return res.status(400).send({error:'User does not exist '});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if(!isMatch){
            return res.status(400).send({error: 'Password incorrect'});
        }
        user.generateToken();
        await user.save();
        res.send({message: 'Login successful', user});
    });

    router.get('/',(req,res) =>{
        let criteria = {status: 'online'};
        User.find(criteria)
            .then(users => res.send(users))
            .catch(()=>res.sendStatus(500));
    });

    router.put('/', async (req, res) => {
        const token = req.get('Authorization');

        if (!token) {
            return res.status(401).send({error: 'Authorization headers not present'});
        }

        const user = await User.findOne({token});

        if (!user) {
            return res.status(401).send({error: 'Token incorrect'});
        }

        user.password = req.body.password;

        await user.save();

        res.sendStatus(200);
    });

router.delete('/sessions', async (req, res)=>{
    const token = req.get('Authorization');
    const success = {message: 'Ok'};


    if (!token) {
        return res.send(success);
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.send(success);
    }

    user.generateToken();
    await user.save();

    res.send(success);

});

module.exports = router;