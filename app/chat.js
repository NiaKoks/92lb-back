const nanoid = require('nanoid');
const  User = require('../models/User');
const Message = require('../models/Message');


const activeConnections = {};
const chat = async (ws, req)=>{
    const token = req.query.token;


    if(!token){
        return ws.close();
    }
    const  user = await User.findOne({token});

    if(!user){
        console.log("closed");
        return ws.close();
    }

    const id = nanoid();
    console.log('client connected! id=', id);
    activeConnections[id] = {user , ws};


    const getUsers = () => {
        return Object.keys(activeConnections).map(connId =>{
            const conn = activeConnections[connId];

            return conn.user.username;
        });
    };

    try {
        const messages = await Message.find().limit(30);
        console.log(messages);
        ws.send(JSON.stringify({
            type: 'LATEST_MESSAGES',
            messages,
        }));
    }catch (e) {
        throw new Error(e)
    }



    Object.keys(activeConnections).map(connId =>{
        const  conn = activeConnections[connId];

        conn.ws.send(JSON.stringify({
            type: 'ACTIVE_USERS',
            usernames: getUsers()
        }))

    });

    ws.on('message', msg =>{
        let decodedMessage;
        try{
            decodedMessage = JSON.parse(msg);
        }catch (e) {
            return console.log('Not valid message ')
        }

        switch (decodedMessage.type) {
            case 'CREATE_MESSAGE':
                Object.keys(activeConnections).forEach(connId =>{
                    const conn = activeConnections[connId].ws;
                    conn.send(JSON.stringify({
                         type: 'NEW_MESSAGE',
                        message:{
                            username: user.username,
                            text: decodedMessage.text
                        }
                    }))
                });

                break;
            default:
                console.log('Not valid message type,' ,decodedMessage.type)
        }

    });


    ws.on('close', msg =>{
        console.log('client disconnected id=', id);
        delete activeConnections[id];

        Object.keys(activeConnections).map(connId =>{
            const  conn = activeConnections[connId];

            conn.ws.send(JSON.stringify({
                type: 'OFFLINE_USER',
                username:user.username
            }))

        });
    });
};

module.exports = chat;