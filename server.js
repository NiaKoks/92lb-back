const  express= require('express');
const expressWS = require('express-ws');
const mongoose = require('mongoose');
const  cors = require('cors');
const config = require('./config');
const users = require('./app/users');
const app = express();
const chat = require('./app/chat');



const port = 8000;

expressWS(app);
app.use(cors());
app.use(express.json());
app.use(express.static('public'));


mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);
    app.ws('/chat',chat);


    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});

